let input = document.getElementById("especie");
let nombres = document.getElementsByClassName("nombre_comun");
especie.addEventListener("keyup", () => {
    let especie = input.value.toLowerCase();
    
    if (especie.trim().length > 0) {
        for (let i = 0; i < nombres.length; i++) {
            const element = nombres[i];
            if (!element.value.toLowerCase().includes(especie)) {
                element.parentElement.style.display = 'none';
            } else {
                if (element.parentElement.style.display == 'none') {
                    element.parentElement.style.display = 'block';
                }
            }
        }   
    } else {
        mostrarTodosEspecies();
    }
});

function mostrarTodosEspecies() {
    for (let i = 0; i < nombres.length; i++) {
        const element = nombres[i];
        element.parentElement.style.display = 'block';
    }
}