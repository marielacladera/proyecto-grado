let input = document.getElementById("usuario");
let nombres = document.getElementsByClassName("names");
usuario.addEventListener("keyup", () => {
    let user = input.value.toLowerCase();
    
    if (user.trim().length > 0) {
        for (let i = 0; i < nombres.length; i++) {
            const element = nombres[i];
            if (!element.value.toLowerCase().includes(user)) {
                element.parentElement.style.display = 'none';
            } else {
                if (element.parentElement.style.display == 'none') {
                    element.parentElement.style.display = 'block';
                }
            }
        }   
    } else {
        mostrarNombres();
    }
});

function mostrarNombres() {
    for (let i = 0; i < nombres.length; i++) {
        const element = nombres[i];
        element.parentElement.style.display = 'block';
    }
}