const adress = 'https://firestore.googleapis.com/v1/projects/friendly-art-304619/databases/(default)/documents/species/';
let nombreComun = document.getElementById('nombre_comun');
const labelNombreComun = document.getElementById('label_nombre_comun');
const idEspecie = atob(document.getElementById('id').value);
const nombre = document.getElementById('nombre_comun');

labelNombreComun.style.display = "block";
labelNombreComun.style.color = "#ff8c00";
labelNombreComun.innerHTML = "Nombre Común:";

nombreComun.addEventListener('change', ()=>{
    fetch(adress)
    .then(response => response.json())
    .then(data => {
        for(let i= 0; i < data.documents.length; i++){
            if(nombreComun.value === data.documents[i].fields.common_name.stringValue){
                if(idEspecie === data.documents[i].fields.id_target.stringValue){
                    labelNombreComun.style.display = "block";
                    labelNombreComun.style.color = "#ff8c00";
                    labelNombreComun.innerHTML = "Nombre Común:";
                }else{
                labelNombreComun.style.display = "block";
                labelNombreComun.style.color = "red";
                labelNombreComun.innerHTML = "*Esta especie ya esta registrada";
                }
                break;
            }else{
                if(nombreComun.value != ''){
                    labelNombreComun.style.display = "block";
                    labelNombreComun.style.color = "#ff8c00";
                    labelNombreComun.innerHTML = "Nombre Común:";
                }else{
                    labelNombreComun.style.display = "block";
                    labelNombreComun.style.color = "#ff8c00";
                    labelNombreComun.innerHTML = "Nombre Común:";
                }
            }
        }
    }) 
})