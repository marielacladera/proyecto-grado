let input = document.getElementById("pregunta");
let nombres = document.getElementsByClassName("question");
pregunta.addEventListener("keyup", () => {
    let question = input.value.toLowerCase();
    
    if (question.trim().length > 0) {
        for (let i = 0; i < nombres.length; i++) {
            const element = nombres[i];
            if (!element.value.toLowerCase().includes(question)) {
                element.parentElement.style.display = 'none';
            } else {
                if (element.parentElement.style.display == 'none') {
                    element.parentElement.style.display = 'block';
                }
            }
        }   
    } else {
        mostrarPreguntas();
    }
});

function mostrarPreguntas() {
    for (let i = 0; i < nombres.length; i++) {
        const element = nombres[i];
        element.parentElement.style.display = 'block';
    }
}