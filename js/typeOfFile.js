const filesSpecie = document.getElementById("multimedia");
const labelfilesSpecie = document.getElementById("multi");

filesSpecie.addEventListener('change',()=>{
    if(filesSpecie.files.length<10){
        let quantImage = 0;
        let quantVideo = 0;
        let quantSound = 0;
        for(let i = 0; i< filesSpecie.files.length; i++){
            if(isImage(filesSpecie.files[i].name)){
                quantImage = quantImage + 1;
            }else if(isVideo(filesSpecie.files[i].name)){
                quantVideo = quantVideo + 1;
            }else if(isSound(filesSpecie.files[i].name)){
                quantSound = quantSound + 1;
            }  else {
                labelfilesSpecie.style.color = "red";
                labelfilesSpecie.innerHTML = "* Seleccione maximo 9 archivos de los cuales: 1 o 3 imagenes pueden ser en formato PNG o JPGE, 1 o 3  sonidos en formato MP3, 1 o 3 videos en formato MP4. Minimo es una imagen, un sonido y un video";
            }
        }
        if(fileQuantity(quantImage) || fileQuantity(quantVideo) || fileQuantity(quantSound)){
            labelfilesSpecie.style.color = "red";
            labelfilesSpecie.innerHTML = "* Seleccione maximo 9 archivos de los cuales: 1 o 3 imagenes pueden ser en formato PNG o JPGE, 1 o 3  sonidos en formato MP3, 1 o 3 videos en formato MP4. Minimo es una imagen, un sonido y un video";
        }
    }else{
       labelfilesSpecie.style.color = "red";
       labelfilesSpecie.innerHTML = "* Seleccione maximo 9 archivos de los cuales: 1 o 3 imagenes pueden ser en formato PNG o JPGE, 1 o 3  sonidos en formato MP3, 1 o 3 videos en formato MP4. Minimo es una imagen, un sonido y un video";
    }
});

function fileQuantity(quantity){
    return quantity == 0 || quantity > 3;
}

function isImage(nameFile){
    return ((/\.(jpg|png)$/i).test(nameFile));
}

function isVideo(nameFile){
    return ((/\.(mp4)$/i).test(nameFile));
}

function isSound(nameFile){
    return((/\.(mp3)$/i).test(nameFile));
}
