const url = 'https://firestore.googleapis.com/v1/projects/friendly-art-304619/databases/(default)/documents/curious_questions/';
let pregunta = document.getElementById('pregunta');
const labelPregunta = document.getElementById('label_pregunta');

pregunta.addEventListener('change', ()=>{
    fetch(url)
    .then(response => response.json())
    .then(data => {
        for(let i= 0; i < data.documents.length; i++){
            if(pregunta.value === data.documents[i].fields.curious_question.stringValue){
                labelPregunta.style.display = "block";
                labelPregunta.style.color = "red";
                labelPregunta.innerHTML = "* Esta Pregunta Curiosa ya esta registrada";
                break;
            }else{
                if(pregunta.value != ''){
                labelPregunta.style.display = "none";
               //labelPregunta.innerHTML = "Pregunta";
               //labelPregunta.style.color = "#ff8c00";
                }else{
                    labelPregunta.style.display = "none";
                }
            }
        }
    })
})