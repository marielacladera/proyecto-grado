const link = 'https://firestore.googleapis.com/v1/projects/friendly-art-304619/databases/(default)/documents/especies/';
let nombreComun = document.getElementById('nombre_comun');
const labelNombreComun = document.getElementById('label_nombre_comun');
nombreComun.addEventListener('change', ()=>{
    fetch(link)
    .then(response => response.json())
    .then(data => {
        for(let i= 0; i < data.documents.length; i++){
            if(nombreComun.value === data.documents[i].fields.nombre_comun.stringValue){
                labelNombreComun.style.display = "block";
                labelNombreComun.style.color = "red";
                labelNombreComun.innerHTML = "* Esta especie ya esta registrada";
                break;
            }else{
                if(nombreComun.value != ''){
                    labelNombreComun.style.display = "none";
                    //labelNombreComun.innerHTML = "Nombre Comun";
                    //labelNombreComun.style.color = "#ff8c00";
                }else{
                    labelNombreComun.style.display = "none";  
                }
            }
        }
    }) 
})