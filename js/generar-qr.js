const boton = document.getElementById("generar");
const qrImg = document.getElementById("qrcodeimg");
const urlQr = document.getElementById("urlQr");
const labelFile = document.getElementById("labelLogo");
// Forms
const formQr = document.getElementById("qr-generator");
const formSpecie = document.getElementById("register-specie");
const nextButton = document.getElementById("next-form");

formSpecie.style.display = 'none';
nextButton.disabled = true;
nextButton.style.display = 'none';

let generateQR = async () => {
    let nameImg = await registerLogoQR();
    if(nameImg == false){ 
        alert("Seleccione una imagen en formato .jpg o .png.");
        
        return false;
    }
    let body = {
        data: nameImg,
        config: {
            body: "square",
            logo: nameImg
        },
        size: 300,
        download: true,
        file: "png"
    }
    let data = await axios.post("https://generator.qrcode.studio/qr/custom", body);
    let rutaQRCode = "https:" + data.data.imageUrl;

    qrImg.setAttribute("src", rutaQRCode);
    urlQr.setAttribute("value", rutaQRCode);
    qrImg.setAttribute("style", "display: block");
    logo.style.display = 'none';
    labelFile.setAttribute("style", "display: none");

    nextButton.style.background = '#ff8c00';
    nextButton.disabled = false;
    nextButton.style.color = '#ffffff';
    nextButton.style.display = 'block'; 

    boton.disabled = true;
    boton.style.background = '#ffffff';
    boton.style.color = '#ff8c00'
    boton.style.display = 'none';
}

let registerLogoQR = async () => {
    const logo = document.getElementById("logo").files[0];
    if (logo == null) return false;
    
    if (!(/\.(jpg|png)$/i).test(logo.name)) return false;
    let formData = new FormData();
    formData.append("file", logo);
    let data = await axios.post("https://generator.qrcode.studio/qr/uploadImage", formData);
    return data.data.file;
}

let nextForm = () => {
    formQr.style.display = 'none';
    formSpecie.style.display = 'block';
}

   
boton.addEventListener('click', generateQR);
nextButton.addEventListener('click', nextForm);