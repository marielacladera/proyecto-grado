const filesImagesCuriosQuestions = document.getElementById("multimedia");
const labelMultimedia = document.getElementById("labelMultimedia");

filesImagesCuriosQuestions.addEventListener('change', ()=>{
    if(filesImagesCuriosQuestions.files.length < 4){
        for(let i = 0; i < filesImagesCuriosQuestions.files.length; i++){
            if(isImage(filesImagesCuriosQuestions.files[i].name)){   
            }else{
                labelMultimedia.style.color = "red";
                labelMultimedia.innerHTML = "* Seleccione maximo 3 imagenes en formato .PNG o .JPG";
                break;
            }
        }
    }else{
        labelMultimedia.style.color = "red";
        labelMultimedia.innerHTML = "* Seleccione maximo 3 imagenes en formato .PNG o .JPG";
    }
})

function isImage(nameFile){
    return ((/\.(jpg|png)$/i).test(nameFile));
}