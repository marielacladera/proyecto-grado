const options = document.getElementById('block-options');
const itemsMenu = document.getElementsByClassName('item-options');

function displayOptions() {
    if (options.style.display === 'block') {
        options.style.display = 'none';
    } else {
        options.style.display = 'block';

        for (let index = 0; index < itemsMenu.length; index++) {
            const element = itemsMenu[index];
            element.style.display = 'none';
        }
    }

}
