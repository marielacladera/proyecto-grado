const qrcodeimg = document.getElementById("qrcode");
const nameqr = document.getElementById("nameqr").value;
const link = document.getElementById("link");
let url = 'https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/' + nameqr + '?alt=media';

qrcodeimg.setAttribute("src", url);
qrcodeimg.style.display = 'block';
qrcodeimg.style.width = '100%';
qrcodeimg.style.height = 'auto';
