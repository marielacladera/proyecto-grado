<?php
    require_once '../vendor/autoload.php';
    use Google\Cloud\Firestore\FirestoreClient;

    class GoogleFirestore {
        protected $db;
        protected $name;

        public function __construct($collection) {
            $this->db = new FirestoreClient([
                'projectId' => 'friendly-art-304619',
                'keyFilePath' => '../key.json'
            ]);
            $this->name = $collection;
        }
        //obtener todos los datos de un documento
        public function getDocuments(){
            $response = array();
            $documents = $this->db->collection($this->name);
            $documents = $documents->documents();
            if($documents) {
                foreach($documents as $document) {
                    if ($document->exists()) {
                        $data = array();
                        $data = $document->data();

                        array_push($data, $document->id());
                        array_push($response, $data);
                    }
                }
            }

            return $response;
        }
        //obtener documento
        public function listDocumentsAtribut($atribute, $value){
            $response = array();
            $documents = $this->db->collection($this->name);
            $query = $documents->where($atribute, '=', $value);
            $documents = $query->documents();
            if($documents) {
                foreach($documents as $document) {
                    if ($document->exists()) {
                        $data = array();
                        $data = $document->data();

                        array_push($data, $document->id());
                        array_push($response, $data);
                    }
                }
            }
            return $response;
        }
        //existe usuario
        public function existUser($name, $dateName, $password, $datePassword, $state, $valor){
            $response = array();
            $documents = $this->db->collection($this->name);
            $query = $documents->where($name, '=', $dateName)->where($password, '=', $datePassword)->where($state, '=', $valor);
            $documents = $query->documents();
            if($documents) {
                foreach($documents as $document) {
                    if ($document->exists()) {
                        $data = array();
                        $data = $document->data();
                        array_push($data, $document->id());
                        array_push($response, $data);
                    }
                }
            }
            return $response;
        }

        //obtener datos de un documento especifico
        public function getDocument($name){
            if($this->db->collection($this->name)->document($name)->snapshot()->exists()) {
                return $this->db->collection($this->name)->document($name)->snapshot()->data();
            }else {
                throw new Exception("No se pudo recuperar el documento");
            }
        }
        //guardar documentos sin id's
        public function saveDocument($data, $nameDocument){
            try {
                $docRef = $this->db->collection($this->name)->document($nameDocument);
                $docRef->set($data);
                
                return "Registro exitoso";
            } catch (\Throwable $th) {
                throw new Exception("No se registro en firestore");
            }
        }
        //guardar documentos sin id's se genera automaticamente
        public function saveDocuments($data){
            try {
                $docRef = $this->db->collection($this->name);
                $docRef->add($data);
                
                return "Registro exitoso";
            } catch (\Throwable $th) {
                throw new Exception("No se registro en firestore");
            }
        }

        //actualizar datos
        public function updateDocument($doc, $data){
            try{
                $docRef = $this->db->collection($this->name)->document($doc);
                $docRef->update($data);
            }catch (\Throwable $th) {
                throw new Exception("No se actualizo en firestore");
            }
        }

        //eliminar un documento
        function deleteDocument($doc){
            try{
                $docRef = $this->db->collection($this->name)->document($doc);
                $docRef->delete();
            }catch (\Throwable $th) {
                throw new Exception("No se elimino en firestore");
            }       
        }
    }