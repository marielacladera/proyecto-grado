<?php
    require_once '../php/GoogleFirestore.php';
    require_once './PrehistorySpecie.php';

    $nombreComun = htmlspecialchars($_REQUEST['nombre_comun']);
    $nombreCientifico = htmlspecialchars($_REQUEST['nombre_cientifico']);
    $era = htmlspecialchars($_REQUEST['era']);
    $genero = htmlspecialchars($_REQUEST['genero']);
    $habitad = htmlspecialchars($_REQUEST['habitad']);
    $peso = htmlspecialchars($_REQUEST['peso']);
    $alto = htmlspecialchars($_REQUEST['alto']);
    $ancho = htmlspecialchars($_REQUEST['ancho']);
    $descripcion = htmlspecialchars($_REQUEST['descripcion']);
    $urlQr = htmlspecialchars($_REQUEST['urlQr']);
    $medPeso = htmlspecialchars($_REQUEST['medPeso']);
    $medAlto = htmlspecialchars($_REQUEST['medAlto']);
    $medAncho = htmlspecialchars($_REQUEST['medAncho']);

    $multimediaNames = $_FILES['multimedia']['name'];
    $multimediaFiles = $_FILES['multimedia']['tmp_name'];
    $multimediaType = $_FILES['multimedia']['type'];

    $qrcodeimage = file_get_contents($urlQr);
    
    if (noEsNulo($nombreComun, 
                 $nombreCientifico, 
                 $era, 
                 $genero, 
                 $habitad, 
                 $peso, 
                 $alto, 
                 $ancho, 
                 $descripcion, 
                 $urlQr, 
                 $qrcodeimage, 
                 $medPeso, 
                 $medAlto, 
                 $medAncho) 
        && runImageType($multimediaType) 
        && isMaxSize($nombreComun, 
                     $nombreCientifico, 
                     $era, 
                     $genero, 
                     $habitad, 
                     $descripcion) 
        && noExistSpecie($nombreComun)) {
        $auxPeso = $peso.' '.$medPeso;
        $auxAncho = $ancho.' '.$medAncho;
        $auxAlto = $alto.' '.$medAlto;
        $multimediaNames = setFileNames($multimediaNames);
        $instanceSpecie = new PrehistorySpecie($nombreComun, $nombreCientifico, $era, $genero, $habitad,
        $auxPeso, $auxAlto, $auxAncho, $descripcion, $urlQr, $qrcodeimage, $multimediaNames, $multimediaFiles);
        $targetId = $instanceSpecie->saveQR($nombreComun, $urlQr);
        $instanceSpecie->saveSpecie($targetId);
        $instanceSpecie->saveFiles();
        $instanceSpecie->saveQrImage($targetId);
        header("Location: ../views/registroEspeciePrehistorica.php?res=exitoso");
    } else { 
        header("Location: ../views/registroEspeciePrehistorica.php?res=error");
    }

    function noEsNulo($nombreComun, $nombreCientifico, $era, $genero, $habitad, $peso,
     $alto, $ancho, $descripcion, $urlQr, $qrcodeimage, $medPeso, $medAlto, $medAncho){
        return $nombreComun != '' && $nombreCientifico != ''  && $era != '' &&  $genero != '' &&
        $habitad != '' && $peso != '' &&  $alto != '' && $ancho != '' && $descripcion != '' && $urlQr != ''
        &&  $qrcodeimage != '' && $medPeso != '' && $medAlto != '' && $medAncho != '';
    }

    function setFileNames($multimediaNames) {
        $setNames = array();
        for ($i = 0; $i < count($multimediaNames); $i++) { 
            $explode = explode(".", $multimediaNames[$i]);
            $sizeExplode = count($explode);
            $ext = $explode[$sizeExplode - 1];
            $name = md5(uniqid(rand(), true)).'.'.$ext;
            array_push($setNames, $name);
        }
        return $setNames;
    }

    function runImageType($multimediaType){
        $res = true;
        $cantidadVideo = 0;
        $cantidadImagen =0;
        $cantidadSonido = 0;
        $cantOtro = 0;
        if(count($multimediaType) < 10){
            for($i = 0; $i < count($multimediaType); $i++){
                if(isImage($multimediaType[$i])){
                    $cantidadImagen = $cantidadImagen + 1;
                }elseif(isVideo($multimediaType[$i])){
                    $cantidadVideo = $cantidadVideo + 1;
                }elseif(isAudio($multimediaType[$i])){
                    $cantidadSonido = $cantidadSonido + 1;
                }else{ 
                  $cantOtro = $cantOtro + 1;
                }  
            }
            if($cantOtro != 0){
                $res = false;
            }elseif(cantArchivo($cantidadImagen) && cantArchivo($cantidadVideo)
            && cantArchivo($cantidadSonido)){
                $res = true;
            }else{
                $res = false;
            }
        }else $res = false;
        return $res;
    }

    function cantArchivo ($cantidad){
        return $cantidad != 0 && $cantidad <= 3;
    }

    function isImage($multimedia){
        return  $multimedia == 'image/jpeg' || $multimedia == 'image/png';
    }
    
    function isVideo($multimedia){
        return  $multimedia == 'video/mp4';
    }
    
    function isAudio($multimedia){
        return  $multimedia== 'audio/mpeg';
    }

    function isMaxSize($nombreComun, $nombreCientifico, $era, $genero, $habitad, $descripcion){
        return strlen($nombreComun) <= 150 && strlen($nombreCientifico) <= 150 && strlen($era) <= 100
        &&  strlen($genero) <= 100 && strlen($habitad) <= 150 && strlen($descripcion) <= 255;
    }

    function noExistSpecie($nombreComun){   
        $instanceFS = new GoogleFirestore('especies');
        $documents = $instanceFS->listDocumentsAtribut('nombre_comun', $nombreComun);
        foreach($documents as $document){
            if($document['nombre_comun'] == $nombreComun ){
                return false;
            }
        }
        return true;
    }