<?php

require_once 'HTTP/Request2.php';
require_once 'SignatureBuilder.php';


class PostNewTarget{

	//Server Keys
	private $access_key 	= "2263d17dd1a9032e24beb7df2edd61c8de6d0b88";
	private $secret_key 	= "d54686a4b63ff4efef2a9120bd4729df756bca62";
	private $targetId 		= "";
	private $url 			= "https://vws.vuforia.com";
	private $requestPath 	= "/targets";
	private $request;       
	private $jsonRequestObject;
	private $targetName 	= "";
	private $imageLocation 	=  "";
	
	function PostNewTarget(){
	}

	function registerQRCode(){
		$this->jsonRequestObject = json_encode( array( 'width'=>320.0 , 'name'=>$this->targetName , 'image'=>$this->getImageAsBase64() , 'application_metadata'=>base64_encode("Vuforia test metadata") , 'active_flag'=>1 ) );
		$this->execPostNewTarget();
	}

	function setTargetName($name) {
		$this->targetName = $name;
	}

	function setImageLocation($image){
		$this->imageLocation = $image;
	}
	
	function getImageAsBase64(){
		$file = file_get_contents( $this->imageLocation );
		if( $file ){
			$file = base64_encode( $file );
		}

		return $file;
	}

	public function execPostNewTarget(){
		$this->request = new HTTP_Request2();
		$this->request->setMethod( HTTP_Request2::METHOD_POST );
		$this->request->setBody( $this->jsonRequestObject );
		$this->request->setConfig(array(
				'ssl_verify_peer' => false
		));
		$this->request->setURL( $this->url . $this->requestPath );
		$this->setHeaders();
		try {
			$response = $this->request->send();
			if (200 == $response->getStatus() || 201 == $response->getStatus() ) {
				$this->targetId = $this->formateTarget($response->getBody());
			} else {
				echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
						$response->getReasonPhrase(). ' ' . $response->getBody();
			}
		} catch (HTTP_Request2_Exception $e) {
			echo 'Error: ' . $e->getMessage();
		}
	}

	public function getTargetId() {
		return $this->targetId;
	}

	private function formateTarget($bodyResponse) {
		$cadena = explode(",", $bodyResponse); 
		$idTarget = $cadena[2];
		$res = explode(":", $idTarget);
		$res = str_replace("}", "", $res[1]);
		$res = str_replace('"', "", $res);

		return $res;
	}

	private function setHeaders(){
		$sb = 	new SignatureBuilder();
		$date = new DateTime("now", new DateTimeZone("GMT"));
		$this->request->setHeader('Date', $date->format("D, d M Y H:i:s") . " GMT" );
		$this->request->setHeader("Content-Type", "application/json" );
		$this->request->setHeader("Authorization" , "VWS " . $this->access_key . ":" . $sb->tmsSignature( $this->request , $this->secret_key ));
	}
}

?>
