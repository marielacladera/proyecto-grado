<?php
    require_once './GoogleFirestore.php';
    session_start();
    $email = htmlspecialchars($_REQUEST['correoElectronico']);
    $password = htmlspecialchars($_REQUEST['password']);

    $instanceFS = new GoogleFirestore('users');
    $user = $instanceFS->existUser('email', $email, 'password', $password, 'state', true);
    if($email == 'vidaprehistorica@life' && $password == 'vida123prehistorica'){
        $_SESSION['email'] = $email;
        $_SESSION['time'] = time();
        header('Location: ../views/listarEspeciePrehistorica.php');
    }else if(count($user)>0){
        $_SESSION['email'] = $user[0]['email'];
        header('Location: ../views/listarEspeciePrehistorica.php');
    }else{
        header('Location: ../views/iniciarSesion.php?res=incorrecto');
    }
