<?php

require_once 'HTTP/Request2.php';
require_once 'SignatureBuilder.php';

// See the Vuforia Web Services Developer API Specification - https://developer.vuforia.com/resources/dev-guide/retrieving-target-cloud-database
// The UpdateTarget sample demonstrates how to update the attributes of a target using a JSON request body. This example updates the target's metadata.

class UpdateTarget{

	//Server Keys
	private $access_key 	= "2263d17dd1a9032e24beb7df2edd61c8de6d0b88";
	private $secret_key 	= "d54686a4b63ff4efef2a9120bd4729df756bca62";

	private $targetId 		= "";
	private $url 			= "https://vws.vuforia.com";
	private $requestPath 	= "/targets/";
	private $request;
	private $jsonBody 		= "";
	private $res = "";
	
	function UpdateTarget(){
	}

	function setTargetId($targetId) {
		$this->targetId = $targetId;
	}	

	function updateNameTarget($name){
		$this->requestPath = $this->requestPath . $this->targetId;
		
		$this->jsonBody = json_encode( array( 'name' => $name) );

		$this->execUpdateTarget();
	}

	public function execUpdateTarget(){

		$this->request = new HTTP_Request2();
		$this->request->setMethod( HTTP_Request2::METHOD_PUT );
		$this->request->setBody( $this->jsonBody );

		$this->request->setConfig(array(
				'ssl_verify_peer' => false
		));

		$this->request->setURL( $this->url . $this->requestPath );

		// Define the Date and Authentication headers
		$this->setHeaders();
		$res = false;

		try {

			$response = $this->request->send();

			if (200 == $response->getStatus()) {
				$this->res = $response->getBody();// $this->formatRes($response->getBody());
			} else {
				echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
						$response->getReasonPhrase(). ' ' . $response->getBody();
			}
		} catch (HTTP_Request2_Exception $e) {
			echo 'Error: ' . $e->getMessage();
		}


	}

	public function getRes() {
		return $this->res;
	}

	/*private function formatRes($response){
		return $response;
	}*/

	private function setHeaders(){
		$sb = 	new SignatureBuilder();
		$date = new DateTime("now", new DateTimeZone("GMT"));

		// Define the Date field using the proper GMT format
		$this->request->setHeader('Date', $date->format("D, d M Y H:i:s") . " GMT" );
		$this->request->setHeader("Content-Type", "application/json" );
		// Generate the Auth field value by concatenating the public server access key w/ the private query signature for this request
		$this->request->setHeader("Authorization" , "VWS " . $this->access_key . ":" . $sb->tmsSignature( $this->request , $this->secret_key ));

	}
}

?>
