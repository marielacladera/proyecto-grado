<?php
    require_once './GoogleFirestore.php';

    $idUser = htmlspecialchars(base64_decode($_REQUEST['id']));
    if($idUser !=''){
        $instanceFS = new GoogleFirestore('users');
        $dataFirestore = [
            ['path' => 'state', 'value' => true]
        ];
        $instanceFS->updateDocument($idUser, $dataFirestore);
        $document = $instanceFS->getDocument($idUser);
        sendEmail($document['name'], $document['last_name'], $document['email']);
        header('Location: ../views/aceptarUsuarios.php?res=seAprobo');
    }else{
        header('Location: ../views/aceptarUsuarios.php?res=noSeAprobo');
    }


    function sendEmail($name, $lastName, $email){ 
            $to = $email;
            $subject = "Registro aceptado VIDA PREHISTORICA";
            $message = "Saludos estimado Sr/Sra $name $lastname se le comunica que su solicitud de 
            registro fue aceptado en la Aplicacion VIDA PREHISTORICA, ya puede iniciar sesion en la 
            aplicacion con su correo y contraseña.";
            $headers = 'From: prueba@gmail.com' . "\r\n" .
            'Reply-To: prueba@gmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail($to, $subject, $message, $headers);
    }