<?php
    require_once '../vendor/autoload.php';
    use Google\Cloud\Storage\StorageClient;

    class GoogleStorage {
        protected $storage;
        protected $bucketName;

        public function __construct() {
            $this->storage = new StorageClient([
                'projectId' => 'friendly-art-304619',
                'keyFilePath' => '../key.json'
            ]);
                
            $this->bucketName = "archivos";
        }

        public function uploadFiles($files, $names){
            $bucket = $this->storage->bucket('friendly-art-304619.appspot.com');
            for ($i=0; $i < count($files); $i++) {    
                $bucket->upload(file_get_contents($files[$i]), [
                    "name" => $names[$i].''
                ]);

                echo '<br>';
                echo 'Imagen subida a storage'.' '.$names[$i];
            }
        }

        public function uploadFile($file, $name){
            $bucket = $this->storage->bucket('friendly-art-304619.appspot.com');
            $bucket->upload($file, [
                "name" => $name.'.png'
            ]);
        }

        public function deleteFile($ref){
            $bucket = $this->storage->bucket('friendly-art-304619.appspot.com');
            $object = $bucket->object($ref);
            try{
                $object->delete();
            }catch(\Throwable $th){
                throw new Exception("No se elimino el archivo");
            }
        }
    }