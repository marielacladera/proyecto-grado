<?php
    require_once './GoogleFirestore.php';

    session_start();
    $email = $_SESSION['email'];

    $currentPassword = htmlspecialchars($_REQUEST['currentPassword']);
    $newPassword = htmlspecialchars($_REQUEST['newPassword']);
    $repeatNewPassword = htmlspecialchars($_REQUEST['repeatNewPassword']);

    if(noEsNulo($currentPassword, $newPassword, $repeatNewPassword) 
       && sonIguales($newPassword, $repeatNewPassword) 
       && tamanoPermitido($newPassword) 
       && noEsEmail($email)){
        $instanceFS = new GoogleFirestore('users');
        $documents = $instanceFS->listDocumentsAtribut('email', $email);   
        if(count($documents)>0){
            $dataFirestore = [
                ['path' => 'password', 'value' => $newPassword]
            ];
            $instanceFS->updateDocument($documents['0']['0'], $dataFirestore);
            session_unset();
            session_destroy();
            header ('Location: ../index.html');
        }else{
            header ('Location: ../views/editarContrasenha.php?res=noSeModifico');
        } 
    }else{
        header ('Location: ../views/editarContrasenha.php?res=noSeModifico');
    }

    function sonIguales($newPassword, $repeatNewPassword){
        return $newPassword == $repeatNewPassword;
    }

    function noEsNulo($currentPassword, $newPassword, $repeatNewPassword)
    {
        return $currentPassword != '' && $newPassword != '' && $repeatNewPassword != '';
    }

    function tamanoPermitido($newPassword){
        return count($newPassword>=9);
    }

    function noEsEmail($email){
        return $email != 'vidaprehistorica@life';
    }