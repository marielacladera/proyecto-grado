<?php
    require_once './GoogleFirestore.php';
    require_once './GoogleStorage.php';
    class CuriousQuestion{
        private $curiousQuestion;
        private $answer;
        private $multimediaNames;
        private $multimediaFiles; 
        private $idCuriousQuestion;

        public function __construct($curiousQuestion, $answer, $multimediaNames, 
        $multimediaFiles, $idCuriousQuestion){
            $this->curiousQuestion = $curiousQuestion;
            $this->answer = $answer;
            $this->multimediaNames = $multimediaNames;
            $this->multimediaFiles = $multimediaFiles;
            $this->idCuriousQuestion = $idCuriousQuestion;
        }

        public function saveCuriousQuestion(){
            $instanceFS = new GoogleFirestore('curious_questions');
            $dataFirestore = [
               "curious_question" => $this->curiousQuestion,
               "answer" => $this->answer,
               "files" => $this->multimediaNames,
               "id_curious_question" => $this->idCuriousQuestion
            ];
            return $instanceFS->saveDocuments($dataFirestore); 
        }

        public function saveFiles() {
            $instance = new GoogleStorage();
            $instance->uploadFiles($this->multimediaFiles, $this->multimediaNames);
        }

        public function getCuriousQuestion($idDocument){
            $instanceFS = new GoogleFirestore('curious_questions');
            $res = $instanceFS->getDocument($idDocument);
            return $res;
        }

        public function deleteFile($arch){
            $instance = new GoogleStorage();
            $res = $instance->deleteFile($arch);
            return $res;
        }

        public function deleteCuriousQuestion($idDocument){
            $instanceFS = new GoogleFirestore('curious_questions');
            $res = $instanceFS->deleteDocument($idDocument);
            return $res;
        }

        public function updateDate($doc){
            $instanceFS = new GoogleFirestore('curious_questions');
            $dataFirestore = [
                ['path' => 'curious_question', 'value' => $this->curiousQuestion],
                ['path' => 'answer', 'value' => $this->answer]
            ];
            return $instanceFS->updateDocument($doc, $dataFirestore);
        }

    }
