<?php
    require_once './GoogleFirestore.php';

    $idUser = htmlspecialchars(base64_decode($_REQUEST['id']));
    if($idUser !=''){
        $instanceFS = new GoogleFirestore('users');
        $document = $instanceFS->getDocument($idUser);
        sendEmail($document['name'], $document['last_name'], $document['email']);
        $res = $instanceFS->deleteDocument($idUser);
        if($res != ''){
            header('Location: ../views/aceptarUsuarios.php?res=seElimino');
        }else{
            header('Location: ../views/aceptarUsuarios.php?res=noElimino');    
        }
    }else{
        header('Location: ../views/aceptarUsuarios.php?res=noElimino');
    }

    function sendEmail($name, $lastName, $email){ 
        $to = $email;
        $subject = "Registro rechazado de  VIDA PREHISTORICA";
        $message = "Saludos estimado Sr/Sra $name $lastname se le comunica que su solicitud de 
        registro fue rechazado en la Aplicacion VIDA PREHISTORICA.";
        $headers = 'From: prueba@gmail.com' . "\r\n" .
        'Reply-To: prueba@gmail.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
}