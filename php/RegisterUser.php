<?php
    require_once './User.php';
    require_once './GoogleFirestore.php';
    require_once '../php/ListUsers.php';

    $name = htmlspecialchars($_REQUEST['nombre']);
    $lastName = htmlspecialchars($_REQUEST['apellido']);
    $email = htmlspecialchars($_REQUEST['correo']);
    $password = htmlspecialchars($_REQUEST['password']);
    $repeatPassword = htmlspecialchars($_REQUEST['repeatPassword']);

    if(isNotNull($name, $lastName, $email, $password, $repeatPassword) && samePassword($password, $repeatPassword) && minSize($name, $lastName, $email, $password)){
        $instanceUser = new User($name, $lastName, $email, $password, false);
        $res = listUsers($email);
        if($res == null){
            $instanceUser->saveUser();
            sendEmail($name, $lastName);
            header("Location: ../views/registrarse.php?res=correcto");
        }else{
            header("Location: ../views/registrarse.php?res=yaEstaRegistrado");
        }
    } else{ 
        header("Location: ../views/registrarUsuario.php?res=faltaDatos");
    }


    function isNotNull($name, $lastName, $email, $password, $repeatPassword){
        return $name != '' && $lastName != '' && $email != '' && $password != '' && $repeatPassword != ''; 
    }

    function samePassword($password, $repeatPassword){
        return $password == $repeatPassword;
    }

    function minSize($name, $lastName, $email, $password){
        return strlen($name) <= 30 && strlen($lastName) <= 50 && strlen($email) <= 50 &&  strlen($password) >= 9;
    }

    function listUsers($email){
        $instanceFS = new GoogleFirestore('users');
        $documents = $instanceFS->listDocumentsAtribut('email', $email);
        return $documents;
    }
    
    function sendEmail($name, $lastName){
        foreach($documents as $document){  
            $to = $document['email'];
            $subject = "Solicitud de aceptacion de registro en la aplicacion VIDA PREHISTORICA";
            $message = "Saludos estimado Administrador\n Le informamos que el Sr/Sra $name $lastname solicito que su 
            registro sea aceptado en la Aplicacion VIDA PREHISTORICA si esta de acuerdo con la solicitud proceda a iniciar sesion 
            con su cuenta a la aplicacion y aceptar caso contrario rechaze la solicitud";
            $headers = 'From: prueba@gmail.com' . "\r\n" .
            'Reply-To: prueba@gmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail($to, $subject, $message, $headers);
        }
    }