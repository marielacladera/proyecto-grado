<?php
    require_once './GoogleFirestore.php';

    $idUser = htmlspecialchars(base64_decode($_REQUEST['id']));
    if($idUser !=''){
        $instanceFS = new GoogleFirestore('users');
        $res = $instanceFS->deleteDocument($idUser);
        if($res != ''){
            header('Location: ../views/listarUsuarios.php?res=noSeElimino');
        }else{
            header('Location: ../views/listarUsuarios.php?res=seElimino');    
        }
    }else{
        header('Location: ../views/listarUsuarios.php?res=noSeElimino');
    }