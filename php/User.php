<?php
require_once './GoogleFirestore.php';
class User{
    private $name;
    private $lastName;
    private $email;
    private $password;
    private $condition;
    
    public function __construct($name, $lastName, $email, $password, $condition){
        $this->name = $name;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->password = $password;
        $this->condition = $condition;
    }
    
    public function saveUser(){
        $instanceFS = new GoogleFirestore('users');
        $dataFirestore = [
           "name" => $this->name,
           "last_name" => $this->lastName,
           "email" => $this->email,
           "password" => $this->password,
           "state" => $this->condition
        ];
        return $instanceFS->saveDocuments($dataFirestore);  
    }

}