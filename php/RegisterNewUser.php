<?php
    require_once './User.php';
    require_once './GoogleFirestore.php';

    $name = htmlspecialchars($_REQUEST['nombre']);
    $lastName = htmlspecialchars($_REQUEST['apellido']);
    $email = htmlspecialchars($_REQUEST['correo']);
    $password = htmlspecialchars($_REQUEST['password']);
    $repeatPassword = htmlspecialchars($_REQUEST['repeatPassword']);

    if(isNotNull($name, $lastName, $email, $password, $repeatPassword) && 
    samePassword($password, $repeatPassword) && minSize($name, $lastName, $email, $password)){
        $instanceUser = new User($name, $lastName, $email, $password, true);
        $res = listUsers($email);
        if($res == null){
            $instanceUser->saveUser();
            sendEmail($name, $lastName, $email, $password);
            header("Location: ../views/registrarUsuario.php?res=correcto");
        }else{
            header("Location: ../views/registrarUsuario.php?res=yaEstaRegistrado");
        }
    } else{ 
        header("Location: ../views/registrarUsuario.php?res=faltaDatos");
    }


    function isNotNull($name, $lastName, $email, $password, $repeatPassword){
        return $name != '' && $lastName != '' && $email != '' && $password != '' && $repeatPassword != ''; 
    }

    function samePassword($password, $repeatPassword){
        return $password == $repeatPassword;
    }

    function minSize($name, $lastName, $email, $password){
        return strlen($name) <= 30 && strlen($lastName) <= 50 && strlen($email) <= 50 &&
        strlen($password) >= 9;
    }

    function listUsers($email){
        $instanceFS = new GoogleFirestore('users');
        $documents = $instanceFS->listDocumentsAtribut('email', $email);
        return $documents;
    }
    
    function sendEmail($name, $lastName, $email){ 
        $to = $email;
        $subject = "Registro VIDA PREHISTORICA";
        $message = "Saludos estimado Sr/Sra $name $lastname se le comunica que fue registrado 
        en la Aplicacion VIDA PREHISTORICA, ya puede iniciar sesion en la 
        aplicacion con su correo: $email y contraseña: $password .";
        $headers = 'From: prueba@gmail.com' . "\r\n" .
        'Reply-To: prueba@gmail.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
}