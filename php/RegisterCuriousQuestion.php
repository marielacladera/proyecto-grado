<?php
    require_once './CuriousQuestion.php';

    $question = htmlspecialchars($_REQUEST['pregunta']);
    $answer = htmlspecialchars($_REQUEST['respuesta']);
    $multimediaNames = $_FILES['multimedia']['name'];
    $multimediaType = $_FILES['multimedia']['type'];
    $multimediaFiles = $_FILES['multimedia']['tmp_name'];

    if(runImageType($multimediaType) && noEsNulo($question, $answer, $multimediaNames, 
    $multimediaType, $multimediaFiles) && isMaxSize($question, $answer)){
        $multimediaNames = setFileNames($multimediaNames);
        $idCuriousQuestion= md5(uniqid(rand(), true));
        $instanceCuriousQuestion = new CuriousQuestion($question, $answer, 
        $multimediaNames, $multimediaFiles, $idCuriousQuestion);
        $instanceCuriousQuestion->saveCuriousQuestion();
        $instanceCuriousQuestion->saveFiles();
        header("Location: ../views/registroPreguntaCuriosa.php?res=exitoso");
    }else{
        header("Location: ../views/registroPreguntaCuriosa.php?res=error");
    }

    function noEsNulo($question, $answer, $multimediaName, $multimediaType, $multimediaFiles){
        return $question != '' && $answer != '' && $multimediaName != '' && $multimediaType != '' && $multimediaFiles != '';
    }

    function  isMaxSize($question, $answer){
        return strlen($question) <= 150 && $answer <= 255;
    }

    function runImageType($multimediaType){
        $res = true;
        if(count($multimediaType)<4){
            $i = 0;
             while($i< count($multimediaType)){
                if(isFile($multimediaType[$i])){
                    $i = $i + 1; 
                }else { 
                    $i = count($multimediaType);
                    $res = false;
                }   
            }
        }else $res = false;
        return $res;
    }

    function isFile($multimedia){
        return  $multimedia== 'image/jpeg' || $multimedia == 'image/png';
    }

    function setFileNames($multimediaNames) {
        $setNames = array();
        for ($i = 0; $i < count($multimediaNames); $i++) { 
            $explode = explode(".", $multimediaNames[$i]);
            $sizeExplode = count($explode);
            $ext = $explode[$sizeExplode - 1];
            $name = md5(uniqid(rand(), true)).'.'.$ext;
            array_push($setNames, $name);
        }
        return $setNames;
    }
    