<?php
    require_once './CuriousQuestion.php';

    $idCurious = htmlspecialchars(base64_decode($_REQUEST['id']));
    $question = htmlspecialchars($_REQUEST['pregunta']);
    $answer = htmlspecialchars($_REQUEST['respuesta']);
    if (noEsNulo($idCurious, $question, $answer) && isMaxSize($question, $answer)) {
        $instanceCurious = new CuriousQuestion($question, $answer, '', '', '');
        $instanceCurious->updateDate($idCurious);
        header("Location: ../views/listarPreguntasCuriosas.php?res=exitoActualizar");
    } else {
        header("Location: ../views/listarPreguntasCuriosas.php?res=errorActualizar");
    }

    function noEsNulo($idCurious, $question, $answer)
    {
        return $idCurious != '' && $question != '' && $answer != '';
    }

    function  isMaxSize($question, $answer){
        return strlen($question) <= 150 && $answer <= 255;
    }