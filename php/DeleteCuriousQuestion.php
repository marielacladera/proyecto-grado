<?php
    require_once '../php/CuriousQuestion.php';

    $id = htmlspecialchars(base64_decode($_REQUEST['id']));
    $curious = new CuriousQuestion('','','','','');
    $res = $curious->getCuriousQuestion($id);
    $files = $res['files'];

    foreach($files as $file){
        $eliminado = $curious->deleteFile($file);
    }
    $r = $curious->deleteCuriousQuestion($id);
    if ($r != ''){
        header("Location: ../views/listarPreguntasCuriosas.php?res=eliminarExitoso");
    }else{
        header("Location: ../views/listarPreguntasCuriosas.php?res=eliminarError" ); 
    }