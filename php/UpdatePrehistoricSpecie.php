<?php
    require_once './PrehistorySpecie.php';

    $idTarget = htmlspecialchars(base64_decode($_REQUEST['id']));
    $nombreComun = htmlspecialchars($_REQUEST['nombre_comun']);
    $nombreCientifico = htmlspecialchars($_REQUEST['nombre_cientifico']);
    $era = htmlspecialchars($_REQUEST['era']);
    $genero = htmlspecialchars($_REQUEST['genero']);
    $habitad = htmlspecialchars($_REQUEST['habitad']);
    $peso = htmlspecialchars($_REQUEST['peso']);
    $alto = htmlspecialchars($_REQUEST['alto']);
    $ancho = htmlspecialchars($_REQUEST['ancho']);
    $descripcion = htmlspecialchars($_REQUEST['descripcion']);
    $medPeso = htmlspecialchars($_REQUEST['medPeso']);
    $medAlto = htmlspecialchars($_REQUEST['medAlto']);
    $medAncho = htmlspecialchars($_REQUEST['medAncho']);

    if (noEsNulo($idTarget, $nombreComun, $nombreCientifico, $era, $genero, $habitad, $peso, $alto, $ancho, $descripcion, $medPeso, $medAlto, $medAncho) && isMaxSize($nombreComun, $nombreCientifico, $era, $genero, $habitad, $descripcion)) {
        $auxPeso = $peso . ' ' . $medPeso;
        $auxAncho = $ancho . ' ' . $medAncho;
        $auxAlto = $alto . ' ' . $medAlto;
        $instanceSpecie = new PrehistorySpecie($nombreComun, $nombreCientifico, $era, $genero, $habitad, $auxPeso, $auxAlto, $auxAncho, $descripcion, '', '', '', '');
        $res = $instanceSpecie->updateNameSpeciePrehistoric($idTarget);
        if($res != ''){
            $instanceSpecie->updateDate($idTarget);
            header("Location: ../views/listarEspeciePrehistorica.php?res=exitoActualizar");
        }else{
            header("Location: ../views/listarEspeciePrehistorica.php?res=errorActualizar");    
        }
    } else {
        header("Location: ../views/listarEspeciePrehistorica.php?res=error");
    }

function noEsNulo($idTarget, $nombreComun, $nombreCientifico, $era, $genero, $habitad, $peso, $alto, $ancho, $descripcion, $medPeso, $medAlto, $medAncho)
{
    return $idTarget != '' && $nombreComun != '' && $nombreCientifico != ''  && $era != '' &&  $genero != '' && $habitad != '' && $peso != '' &&  $alto != '' && $ancho != '' && $descripcion != ''  && $medPeso != '' && $medAlto != '' && $medAncho != '';
}

function isMaxSize($nombreComun, $nombreCientifico, $era, $genero, $habitad, $descripcion){
    return strlen($nombreComun) <= 150 && strlen($nombreCientifico) <= 150 && strlen($era) <= 100 &&  strlen($genero) <= 100 && strlen($habitad) <= 150 && strlen($descripcion) <= 255;
}