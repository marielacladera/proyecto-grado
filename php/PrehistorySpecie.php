<?php
    require_once './PostNewTarget.php';
    require_once './UpdateTarget.php';
    require_once './DeleteTarget.php';
    require_once './GoogleFirestore.php';
    require_once './GoogleStorage.php';

    class PrehistorySpecie{
        private $commonName;
        private $scientificName;
        private $era;
        private $gender;
        private $habitad;
        private $weight;
        private $high;
        private $width;
        private $description;
        private $urlQr;
        private $qrcodeImage;
        private $multimediaNames;
        private $multimediaFiles;

        public function __construct($commonName, $scientificName, $was, $gender, $habitad, $weight, $high,
        $width, $description, $urlQr, $qrcodeImage,  $multimediaNames, $multimediaFiles) {
            $this->commonName = $commonName;
            $this->scientificName = $scientificName;
            $this->was = $was;
            $this->gender = $gender;
            $this->habitad = $habitad;
            $this->weight = $weight;
            $this->high = $high;
            $this->width = $width;
            $this->description = $description;
            $this->urlQr = $urlQr;
            $this->qrcodeImage = $qrcodeImage;
            $this->multimediaNames = $multimediaNames;
            $this->multimediaFiles = $multimediaFiles;
        }

        public function saveSpecie($targetId) {
            $instanceFS = new GoogleFirestore('species');
            $dataFirestore = [
                "common_name" => $this->commonName,
                "scientific_name" => $this->scientificName,
                "was" => $this->was,
                "gender" => $this->gender,
                "habitad" => $this->habitad,
                "weight" => $this->weight,
                "high" => $this->high,
                "width" => $this->width,
                "description" => $this->description,
                "files" => $this->multimediaNames,
                "id_target" => $targetId
            ];
            
            return $instanceFS->saveDocument($dataFirestore, $targetId); 
        }
    
        public function saveQR() {
            $instance = new PostNewTarget();
            $instance->setTargetName($this->commonName);
            $instance->setImageLocation($this->urlQr);
            $instance->registerQRCode();
            $targetId = $instance->getTargetId();

            return $targetId;
        }

        public function updateNameSpeciePrehistoric($targetId){
            $instance = new UpdateTarget();
            $instance->setTargetId($targetId);
            $instance->updateNameTarget($this->commonName);
            $res =  $instance->getRes();
            return $res;
        }

        public function deleteTargetPrehitorySpecie($targetId){
            $instance = new DeleteTarget();
            $instance->setTargetId($targetId);
            $instance->eliminarTarget();
            $res= $instance->getRes();
        }

        public function updateDate($targetId){
            $instanceFS = new GoogleFirestore('species');
            $dataFirestore = [
                ['path' => 'common_name', 'value' => $this->commonName],
                ['path' => 'scientific_name', 'value' => $this->scientificName],
                ['path' => 'was', 'value' => $this->was],
                ['path' => 'gender', 'value' => $this->gender],
                ['path' => 'habitad', 'value'  => $this->habitad],
                ['path' => 'weight', 'value' => $this->weight],
                ['path' => 'high', 'value' => $this->high],
                ['path' => 'width', 'value' => $this->width],
                ['path' => 'description', 'value' => $this->description]
            ];
            return $instanceFS->updateDocument($targetId, $dataFirestore);
        }

        public function saveFiles() {
            $instance = new GoogleStorage();
            $instance->uploadFiles($this->multimediaFiles, $this->multimediaNames);
        }

        public function deleteFile($arch){
            $instance = new GoogleStorage();
            $res = $instance->deleteFile($arch);
            return $res;
        }

        public function saveQrImage($targetId) {
            $instance = new GoogleStorage();
            $instance->uploadFile($this->qrcodeImage, $targetId);
        }

        public function getPrehistorySpecie($targetId){
            $instanceFS = new GoogleFirestore('species');
            $res = $instanceFS->getDocument($targetId);
            return $res;
        }

        public function deletePrehistorySpecie($idDocument){
            $instanceFS = new GoogleFirestore('species');
            $res = $instanceFS->deleteDocument($idDocument);
            return $res;
        }
    }