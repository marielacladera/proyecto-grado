<?php
    require_once '../php/PrehistorySpecie.php';

    $id = htmlspecialchars(base64_decode($_REQUEST['id']));
    if(eliminar($id)){
        header("Location: ../views/listarEspeciePrehistorica.php?res=exitosoEliminar" ); 
    }else{
        header("Location: ../views/listarEspeciePrehistorica.php?res=errorEliminar" ); 
    }
function eliminar($id){
    try{
        $instance = new PrehistorySpecie('', '', '', '', '', '', '', '', '', '', '', '', '');
        $arch = $instance->getPrehistorySpecie($id);
        $files = $arch['files'];
        $deleteVuforia = $instance->deleteTargetPrehitorySpecie($id);
        $deleteImg = $instance->deleteFile($id.'.png');
        foreach($files as $file){
            $deleteFile = $instance->deleteFile($file);
        }
        $deleteData = $instance->deletePrehistorySpecie($id);
        return true;
    }catch(\Throwable $th){
        throw new Exception("No se elimino el archivo");
    }
    
}
    
    