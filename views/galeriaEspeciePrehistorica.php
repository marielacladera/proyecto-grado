<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/favicon.png" type="image/png">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/galery.css">
    <link rel="stylesheet" href="../css/stylenav.css">
    <title>VidaPrehistorica-GaleriaEspeciePrehistorica</title>
</head>

<body>
    <?php
        session_start();
        if(!$_SESSION['email'])
        {
            header('Location: ../index.html');
        }else{
			if((time() - $_SESSION['time']) > 1800){
				header('location: ../php/ClosedSesion.php');
			}
		}
    ?>
    <header>      
        <span class="nav-bar" id="btnMenu"><i class="fas fa-bars"></i></span>
        <nav class="main-nav">
            <ul class="menu" id="menu">
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Mi Cuenta <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="editarContrasenha.php" class="menu-link">Editar Contraseña</a></li>
                        <li class="menu-item"><a href="../php/ClosedSesion.php" class="menu-link">Salir</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Usuarios <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="registrarUsuario.php" class="menu-link">Registrar Nuevo Usuario</a></li>
                        <li class="menu-item"><a href="aceptarUsuarios.php" class="menu-link">Aceptar Usuarios</a></li>
                        <li class="menu-item"><a href="listarUsuarios.php" class="menu-link">Listar Usuarios</a></li>
                    </ul>
                </li>
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Especies Prehistóricas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarEspeciePrehistorica.php" class="menu-link">Listar Especies Prehistóricas</a></li>
                        <li class="menu-item"><a href="registroEspeciePrehistorica.php" class="menu-link">Registrar Especie Prehistórica</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Preguntas Curiosas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarPreguntasCuriosas.php" class="menu-link">Listar Preguntas Curiosas</a></li>
                        <li class="menu-item"><a href="registroPreguntaCuriosa.php" class="menu-link">Registrar Pregunta Curiosa</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <section  class="containerGalery">
        <?php
            $id = htmlspecialchars(base64_decode($_REQUEST['id']));
            require_once '../php/ListDataPrehistoricSpecie.php';
        ?>
        <h2>Galeria Especie Prehistorica - <?php echo $document['common_name'];?></h2>
        <?php 
            $archivos = $document['files'];
            foreach($archivos as $archivo){
                $ext = explode(".", $archivo); 
                if($ext[1] == 'png' || $ext[1]== 'jpg'){
            ?>
            <div class="imageGalery">
                <img src="<?php echo'https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/'.$archivo.'?alt=media';?>" alt="media">
            </div>
            <?php 
                } elseif($ext[1] == 'mp4'){
            ?>
            <div class="imageGalery">
                <video width="250" controls>
                    <source src="<?php echo'https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/'.$archivo.'?alt=media';?>"type="video/mp4">
                </video>
            </div>         
            <?php }else{
            ?>
            <div class="imageGalery">
                <audio controls>
                    <source src="<?php echo'https://firebasestorage.googleapis.com/v0/b/friendly-art-304619.appspot.com/o/'.$archivo.'?alt=media';?>" type="audio/mpeg">
                </audio>
            </div> 
            <?php }      
            }
        ?>     
    </section>
    <script src="../js/interactiveMenu.js"></script>
</body>
</html>