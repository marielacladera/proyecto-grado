<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/favicon.png" type="image/png">
    <link rel="stylesheet" href="../css/forms.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/stylenav.css">
    <title>VidaPrehistorica-RegistroEspeciePrehistorica</title>
</head>
<body>
    <?php
        session_start();
        if(!$_SESSION['email'])
        {
            header('Location: ../index.html');
        }else{
			if((time() - $_SESSION['time']) > 1800){
				header('location: ../php/ClosedSesion.php');
			}
		}
    ?>
    <header>      
        <span class="nav-bar" id="btnMenu"><i class="fas fa-bars"></i></span>
        <nav class="main-nav">
            <ul class="menu" id="menu">
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Mi Cuenta <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="editarContrasenha.php" class="menu-link">Editar Contraseña</a></li>
                        <li class="menu-item"><a href="../php/ClosedSesion.php" class="menu-link">Salir</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Usuarios <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="registrarUsuario.php" class="menu-link">Registrar Nuevo Usuario</a></li>
                        <li class="menu-item"><a href="aceptarUsuarios.php" class="menu-link">Aceptar Usuarios</a></li>
                        <li class="menu-item"><a href="listarUsuarios.php" class="menu-link">Listar Usuarios</a></li>
                    </ul>
                </li>
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Especies Prehistóricas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarEspeciePrehistorica.php" class="menu-link">Listar Especies Prehistóricas</a></li>
                        <li class="menu-item"><a href="registroEspeciePrehistorica.php" class="menu-link">Registrar Especie Prehistórica</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Preguntas Curiosas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarPreguntasCuriosas.php" class="menu-link">Listar Preguntas Curiosas</a></li>
                        <li class="menu-item"><a href="registroPreguntaCuriosa.php" class="menu-link">Registrar Pregunta Curiosa</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <section class="container">
        <div class="formulario">
            <h2>Registro de Especie Prehistórica</h2>
            <div class="inputs">
                <form action="../php/RegisterPrehistorySpecie.php" enctype="multipart/form-data" method="POST">
                    <div id="qr-generator">
                        <input type="hidden" id="urlQr" name="urlQr">
                        <label class="etiqueta" for="logo" id="labelLogo">Selecciona una imagen para generar el codigo QR:</label><br>
                        <img id="qrcodeimg" alt="QRCode"><br>
                        <input type="file" name="logo" id="logo" class="file-form" accept="image/jpeg, image/png"><br>
                        <input type="button" id="generar" value="Generar Codigo QR" class="submit"><br>
                        <input type="button" id="next-form" value="Siguiente" class="next-form"><br>
                    </div>
                    <div id="register-specie">
                        <label for="nombre_comun" id='label_nombre_comun'></label>
                        <input type="text" id="nombre_comun" name="nombre_comun" placeholder="Nombre Común"
                            class="inputs-form" autofocus="1" autocomplete="off" required><br>
                        <input type="text" id="nombre_cientifico" name="nombre_cientifico"
                            placeholder="Nombre Científico" class="inputs-form" autofocus="1" autocomplete="off"
                            required><br>
                        <input type="text" id="era" name="era" placeholder="Era" class="inputs-form" autofocus="1"
                            autocomplete="off" required><br>
                        <input type="text" id="genero" name="genero" placeholder="Género" class="inputs-form"
                            autofocus="1" autocomplete="off" required><br>
                        <input type="text" id="habitad" name="habitad" placeholder="Habitad" class="inputs-form"
                            autofocus="1" autocomplete="off" required><br>
                        <input type="number" id="peso" name="peso" placeholder="Peso" class="inputs-f" autofocus="1"
                            autocomplete="off" min="0" step="any" required>
                        <select name="medPeso" id="medPeso" class="select-form">
                            <option value="kg">Kilogramo(kg)</option>
                            <option value="t">Tonelada(t)</option>
                        </select>    
                        <br>
                        <input type="number" id="ancho" name="ancho" placeholder="Ancho" class="inputs-f"
                            autofocus="1" autocomplete="off" min="0" step="any" required>
                        <select name="medAncho" id="medAncho" class="select-form">
                            <option value="cm">Centimetro(cm)</option>
                            <option value="m">Metro(m)</option>
                        </select> 
                        <br>
                        <input type="number" id="alto" name="alto" placeholder="Alto" class="inputs-f" autofocus="1"
                            autocomplete="off" min="0" step="any" required>
                        <select name="medAlto" id="medAlto" class="select-form">
                            <option value="cm">Centimetro(cm)</option>
                            <option value="m">Metro(m)</option>
                        </select> 
                        <br>
                        <textarea name="descripcion" id="descripcion" cols="36" rows="7" placeholder="Descripción(Max 255 caracteres)"
                            class="inputs-form" autofocus="1" autocomplete="off" required></textarea><br>
                        <label class="etiqueta" for="multimedia" id="multi">Selecciona los archivos multimedia (minimo 3 archivos: 1 imagen .JPGE o .PNG, 1 video .MP4, 1 sonido .MP3; maximo 9 archivos: 3 imagenes .JPGE o .PNG , 3 videos .MP4, 3 sonidos .MP3):</label><br>
                        <input type="file" name="multimedia[]" multiple="multiple" id="multimedia"
                            class="file-form" accept="image/jpeg, image/png, video/mp4, audio/mpeg"><br>
                        <input type="submit" id="registrar" value="Registrar" class="submit" autofocus="1">
                    </div>
                </form>
            </div>
        </div>
        <?php
            if($_REQUEST) {
                $res = $_REQUEST['res'];
                if ($res == 'exitoso') {
                ?>
                    <script>
                        alert('Registro exitoso')
                    </script>
                <?php 
                } else { ?>
                    <script>
                        alert('No se registro')
                    </script>
                <?php 
                }
            }
        ?>
    </section>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="../js/generar-qr.js"></script>
    <script src="../js/interactiveMenu.js"></script>
    <script src="../js/typeOfFile.js"></script>
    <script src="../js/inputValidator.js"></script>
    <script src="../js/getSpecies.js"></script>
</body>
</html>