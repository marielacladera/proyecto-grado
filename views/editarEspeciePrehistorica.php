<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/favicon.png" type="image/png">
    <link rel="stylesheet" href="../css/forms.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/stylenav.css">
    <title>VidaPrehistorica-EditarEspeciePrehistorica</title>
</head>
<body>
    <?php
        session_start();
        if(!$_SESSION['email'])
        {
            header('Location: ../index.html');
        }else{
			if((time() - $_SESSION['time']) > 1800){
				header('location: ../php/ClosedSesion.php');
			}
		}
    ?>
    <header>      
        <span class="nav-bar" id="btnMenu"><i class="fas fa-bars"></i></span>
        <nav class="main-nav">
            <ul class="menu" id="menu">
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Mi Cuenta <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="editarContrasenha.php" class="menu-link">Editar Contraseña</a></li>
                        <li class="menu-item"><a href="../php/ClosedSesion.php" class="menu-link">Salir</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Usuarios <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="registrarUsuario.php" class="menu-link">Registrar Nuevo Usuario</a></li>
                        <li class="menu-item"><a href="aceptarUsuarios.php" class="menu-link">Aceptar Usuarios</a></li>
                        <li class="menu-item"><a href="listarUsuarios.php" class="menu-link">Listar Usuarios</a></li>
                    </ul>
                </li>
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Especies Prehistóricas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarEspeciePrehistorica.php" class="menu-link">Listar Especies Prehistóricas</a></li>
                        <li class="menu-item"><a href="registroEspeciePrehistorica.php" class="menu-link">Registrar Especie Prehistórica</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Preguntas Curiosas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarPreguntasCuriosas.php" class="menu-link">Listar Preguntas Curiosas</a></li>
                        <li class="menu-item"><a href="registroPreguntaCuriosa.php" class="menu-link">Registrar Pregunta Curiosa</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <section class="container">
        <div class="formulario">
            <h2>Editar datos de Especie Prehistórica</h2>
            <?php
            $id = htmlspecialchars(base64_decode($_REQUEST['id']));
            require_once '../php/ListDataPrehistoricSpecie.php';
            ?>
            <div class="inputs">
                <form action="../php/UpdatePrehistoricSpecie.php" method="POST">
                    <input type="hidden" name="id" id="id" value=<?php echo base64_encode($id); ?>>
                    <input type="hidden" id="nameqr" value="<?php echo $id . '.png' ?>">
                    <img alt="qrcodeimg" id="qrcode">
                    <label for="nombre_comun" id="label_nombre_comun">Nombre Común:</label>
                    <input type="text" id="nombre_comun" name="nombre_comun" value="<?php echo $document['common_name']; ?>" class="inputs-form" autofocus="1" autocomplete="off" required><br>
                    <label for="nombre_cientifico">Nombre Científico:</label>
                    <input type="text" id="nombre_cientifico" name="nombre_cientifico" value="<?php echo $document['scientific_name']; ?>" class="inputs-form" autofocus="1" autocomplete="off" required><br>
                    <label for="era">Era:</label>
                    <input type="text" id="era" name="era" value="<?php echo $document['was']; ?>" class="inputs-form" autofocus="1" autocomplete="off" required><br>
                    <label for="genero">Género:</label>
                    <input type="text" id="genero" name="genero" value="<?php echo $document['gender']; ?>" class="inputs-form" autofocus="1" autocomplete="off" required><br>
                    <label for="habitad">Habitad:</label>
                    <input type="text" id="habitad" name="habitad" value="<?php echo $document['habitad']; ?>" class="inputs-form" autofocus="1" autocomplete="off" required><br>
                    <label for="peso">Peso:</label><br>
                    <?php
                    $cadenaPeso = explode(" ", $document['weight']);
                    ?>
                    <input type="number" id="peso" name="peso" value="<?php echo $cadenaPeso[0]; ?>" class="inputs-f" autofocus="1" autocomplete="off" min="0" step="any" required>
                    <select name="medPeso" id="medPeso" class="select-form">
                        <option value="kg">Kilogramo(kg)</option>
                        <option value="t">Tonelada(t)</option>
                    </select> <br>
                    <label for="ancho">Ancho:</label><br>
                    <?php
                    $cadenaAncho = explode(" ", $document['width']);
                    ?>
                    <input type="number" id="ancho" name="ancho" value="<?php echo $cadenaAncho[0]; ?>" class="inputs-f" autofocus="1" autocomplete="off" min="0"  step="any"required>
                    <select name="medAncho" id="medAncho" class="select-form">
                        <option value="cm">Centimetro(cm)</option>
                        <option value="m">Metro(m)</option>
                    </select> <br>
                    <label for="alto" \>Alto:</label><br>
                    <?php
                    $cadenaAlto = explode(" ", $document['high']);
                    ?>
                    <input type="number" id="alto" name="alto" value="<?php echo $cadenaAlto[0]; ?>" class="inputs-f" autofocus="1" autocomplete="off" min="0" step="any" required>
                    <select name="medAlto" id="medAlto" class="select-form">
                        <option value="cm">Centimetro(cm)</option>
                        <option value="m">Metro(m)</option>
                    </select> <br>
                    <label for="descripcion">Descripción:</label>
                    <textarea name="descripcion" id="descripcion" cols="36" rows="5" placeholder="Descripcion" class="inputs-form" autofocus="1" autocomplete="off" required><?php echo $document['description']; ?></textarea><br>
                    <input type="submit" value="Guardar Cambios" class="submit" autofocus="1">
                </form>
            </div>
        </div>
    </section> 
    <script src="../js/interactiveMenu.js"></script>
    <script src="../js/loadqrcode.js"></script>
    <script src="../js/inputValidator.js"></script>
    <script src="../js/getSpecie.js"></script>
</body>

</html>