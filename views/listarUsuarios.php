<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/favicon.png" type="image/png">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/stylenav.css">
    <title>VidaPrehistorica-ListarUsuarios</title>
</head>
<body>
    <?php
        session_start();
        if(!$_SESSION['email'])
        {
            header('Location: ../index.html');
        }else{
			if((time() - $_SESSION['time']) > 1800){
				header('location: ../php/ClosedSesion.php');
			}
		}
    ?>
    <header>      
        <span class="nav-bar" id="btnMenu"><i class="fas fa-bars"></i></span>
        <nav class="main-nav">
            <ul class="menu" id="menu">
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Mi Cuenta <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="editarContrasenha.php" class="menu-link">Editar Contraseña</a></li>
                        <li class="menu-item"><a href="../php/ClosedSesion.php" class="menu-link">Salir</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Usuarios <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="registrarUsuario.php" class="menu-link">Registrar Nuevo Usuario</a></li>
                        <li class="menu-item"><a href="aceptarUsuarios.php" class="menu-link">Aceptar Usuarios</a></li>
                        <li class="menu-item"><a href="listarUsuarios.php" class="menu-link">Listar Usuarios</a></li>
                    </ul>
                </li>
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Especies Prehistóricas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarEspeciePrehistorica.php" class="menu-link">Listar Especies Prehistóricas</a></li>
                        <li class="menu-item"><a href="registroEspeciePrehistorica.php" class="menu-link">Registrar Especie Prehistórica</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Preguntas Curiosas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarPreguntasCuriosas.php" class="menu-link">Listar Preguntas Curiosas</a></li>
                        <li class="menu-item"><a href="registroPreguntaCuriosa.php" class="menu-link">Registrar Pregunta Curiosa</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <section>
        <h2>Lista de Usuarios Aceptados</h2>
        <input class="buscador" type="text" name="usuario" id="usuario" placeholder="Buscar por el nombre del usuario">    
        <div class ="content">
            <?php 
                require_once '../php/ListUsers.php';
                foreach($documents as $document){
                    if($document['email'] != $_SESSION['email'] ){
            ?>
            <div class="cards">
                <br>
                <p><strong>Nombre:</strong></p>
                <p><?php echo $document['name']; ?></p>
                <input type="hidden" class="names" value="<?php echo $document['name']; ?>">
                <p><strong>Apellido:</strong></p>
                <p><?php echo $document['last_name']; ?></p>
                <p><strong>Correo Electrónico:</strong></p>
                <p><?php echo $document['email']; ?></p>
                <div class="accionesUsuarios">   
                    <a href="../php/DeleteUser.php?id=<?php echo base64_encode($document[0]);?>"><img src="../img/basura.png" alt="eliminar" title="Eliminar"></a>
                </div>
            </div>
            <?php
                    }
                }
            ?>       
        </div>
        <?php
        if($_REQUEST) {
            $res = $_REQUEST['res'];
            if($res == 'seElimino'){ ?>
                <script>
                    alert('Se elimino al usuario exitosamente.')
                </script>
             <?php
             }elseif($res == 'noSeElimino'){ ?>
                <script>
                    alert('No se elimino al usuario.')
                </script>
        <?php
            }
        }
       ?>
    </section>
     <script src="../js/interactiveMenu.js"></script>
     <script src="../js/buscarUsuario.js"></script>
</body>
</html>
