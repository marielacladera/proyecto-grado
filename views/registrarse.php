<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/favicon.png" type="image/png">
    <link rel="stylesheet" href="../css/forms.css">
    <link rel="stylesheet" href="../css/style.css">
    <title>VidaPrehistorica-Registrarse</title>
</head>

<body>
    <section class="container">
        <div class="formulario">
            <a href="../index.html"><img class="logo" src="../img/favicon.png" alt="logoP" width="" height=""></a>
            <h2>Registrarse</h2>
            <div class="inputs">
                <form action="../php/RegisterUser.php">
                    <input type="text" placeholder="Nombre" class="inputs-form" id="nombre" name="nombre" autofocus="1"
                        autocomplete="off" required><br>
                    <input type="text" placeholder="Apellido" class="inputs-form" id="apellido" name="apellido"
                        autofocus="1" autocomplete="off" required><br>
                    <input type="email" placeholder="Correo Electrónico" class="inputs-form" id="correo" name="correo"
                        autofocus="1" autocomplete="off" required><br>
                    <input type="password" placeholder="Contraseña mínimo 9 caracteres" class="inputs-form" id="password" name="password"
                        autofocus="1" autocomplete="off" required><br>
                    <input type="password" placeholder="Repetir contraseña" class="inputs-form" id="repeatPassword"
                        name="repeatPassword" autofocus="1" autocomplete="off" required><br>
                    <input type="submit" value="Registrarse" class="submit" autofocus="1">
                </form>
            </div>
        </div>
        <?php
        if($_REQUEST) {
            $res = $_REQUEST['res'];
            if ($res == 'correcto') {
            ?>
                <script>
                    alert('Registro exitoso en espera de aceptación')
                </script>
            <?php 
            } elseif($res == 'yaEstaRegistrado'){ ?>
                <script>
                    alert('El correo ya está siendo utilizado intente con otro correo')
                </script>
            <?php 
            }elseif($res == 'faltaDatos'){ ?>
                <script>
                    alert('Ingrese todos los campos vacíos')
                </script>
            <?php
            }
        }
    ?>
    </section>
    <div class="enlaces">
        <p>Si tiene una cuenta? <a href="iniciarSesion.php">Inicia Sesión</a></p>
    </div>
    <script src="../js/inputValidator.js"></script>
</body>
</html>