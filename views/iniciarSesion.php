<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/favicon.png" type="image/png">
    <link rel="stylesheet" href="../css/forms.css">
    <link rel="stylesheet" href="../css/style.css">
    <title>VidaPrehistorica-IniciarSesion</title>
</head>

<body>
    <section class="container">
        <div class="formulario">
            <a href="../index.html"><img class="logo" src="../img/favicon.png" alt="logoP" width="" height=""></a>
            <h2>Iniciar Sesión</h2>
            <div class="inputs">
                <form action="../php/StartSesion.php">
                    <input type="text" placeholder="Correo electrónico" class="inputs-form"
                        id="correoElectronico" name="correoElectronico" autofocus="1" autocomplete="off" required><br>
                    <input type="password" placeholder="Contraseña" class="inputs-form" id="password" name="password"
                        autofocus="1" autocomplete="off" required><br>
                    <input type="submit" value="Ingresar" class="submit" autofocus="1">
                </form>
            </div>
        </div>
        <?php
        if($_REQUEST) {
            $res = $_REQUEST['res'];
            if ($res == 'incorrecto') {
                ?>
                    <script>
                        alert('Usuario Invalido')
                    </script>
                <?php 
            }
        }
        ?>
    </section>
    <div class="enlaces">
        <p>Aun no tiene cuenta? <a href="registrarse.php">Registrate</a></p>
    </div>
    <script src="../js/inputValidator.js"></script>
</body>
</html>