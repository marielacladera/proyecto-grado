<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/favicon.png" type="image/png">
    <link rel="stylesheet" href="../css/forms.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/stylenav.css">
    <title>VidaPrehistorica-RegistrarNuevoUsuario</title>
</head>

<body>
    <?php
        session_start();
        if(!$_SESSION['email'])
        {
            header('Location: ../index.html');
        }else{
			if((time() - $_SESSION['time']) > 1800){
				header('location: ../php/ClosedSesion.php');
			}
		}
    ?>
     <header>      
        <span class="nav-bar" id="btnMenu"><i class="fas fa-bars"></i></span>
        <nav class="main-nav">
            <ul class="menu" id="menu">
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Mi Cuenta <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="editarContrasenha.php" class="menu-link">Editar Contraseña</a></li>
                        <li class="menu-item"><a href="../php/ClosedSesion.php" class="menu-link">Salir</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Usuarios <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="registrarUsuario.php" class="menu-link">Registrar Nuevo Usuario</a></li>
                        <li class="menu-item"><a href="aceptarUsuarios.php" class="menu-link">Aceptar Usuarios</a></li>
                        <li class="menu-item"><a href="listarUsuarios.php" class="menu-link">Listar Usuarios</a></li>
                    </ul>
                </li>
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Especies Prehistóricas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarEspeciePrehistorica.php" class="menu-link">Listar Especies Prehistóricas</a></li>
                        <li class="menu-item"><a href="registroEspeciePrehistorica.php" class="menu-link">Registrar Especie Prehistóricas</a></li>
                    </ul>
                </li> 
                <li class="menu-item container-submenu">
                    <a href="#" class="menu-link submenu-btn">Preguntas Curiosas <i class="fas fa-angle-down"></i></a>
                    <ul class="submenu">
                        <li class="menu-item"><a href="listarPreguntasCuriosas.php" class="menu-link">Listar Preguntas Curiosas</a></li>
                        <li class="menu-item"><a href="registroPreguntaCuriosa.php" class="menu-link">Registrar Pregunta Curiosa</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <section class="container">
        <div class="formulario">
            <h2>Registrar Nuevo Usuario</h2>
            <div class="inputs">
                <form action="../php/RegisterNewUser.php">
                    <input type="text" placeholder="Nombre" class="inputs-form" id="nombre" name="nombre" autofocus="1"
                        autocomplete="off" required><br>
                    <input type="text" placeholder="Apellido" class="inputs-form" id="apellido" name="apellido"
                        autofocus="1" autocomplete="off" required><br>
                    <input type="email" placeholder="Correo Electrónico" class="inputs-form" id="correo" name="correo"
                        autofocus="1" autocomplete="off" required><br>
                    <input type="password" placeholder="Contraseña mínimo 9 caracteres" class="inputs-form" id="password" name="password"
                        autofocus="1" autocomplete="off" required><br>
                    <input type="password" placeholder="Repetir contraseña" class="inputs-form" id="repeatPassword"
                        name="repeatPassword" autofocus="1" autocomplete="off" required><br>
                    <input type="submit" value="Registrar" class="submit" autofocus="1">
                </form>
            </div>
        </div>
        <?php
        if($_REQUEST) {
            $res = $_REQUEST['res'];
            if ($res == 'correcto') {
            ?>
                <script>
                    alert('Registro exitoso')
                </script>
            <?php 
            } elseif($res == 'yaEstaRegistrado'){ ?>
                <script>
                    alert('El correo ya esta siendo utilizado intente con otro correo')
                </script>
            <?php 
            }elseif($res == 'faltaDatos'){ ?>
                <script>
                    alert('Ingrese todos los campos vacios')
                </script>
            <?php
            }
        }
    ?>
    </section>
    <script src="../js/interactiveMenu.js"></script>
    <script src="../js/inputValidator.js"></script>
</body>
</html>